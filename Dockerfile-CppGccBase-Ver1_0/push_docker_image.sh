#! /bin/bash

if [ $# -ne 1 ]; then
  echo "The number of specified arguments is $#." 1>&2
  echo "USAGE: ./push_docker_image.sh v1.0" 1>&2
  exit 1
fi

docker push zigzagzackey/cpp_gcc_base:$1