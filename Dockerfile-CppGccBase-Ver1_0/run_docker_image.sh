#! /bin/bash

if [ $# -ne 2 ]; then
  echo "The number of specified arguments is $#." 1>&2
  echo "USAGE: ./run_docker_image.sh v1.0 /tmp/DockerAttach:/root" 1>&2
  exit 1
fi

docker run -it -v $2 -e DISPLAY="172.17.0.2:0" zigzagzackey/cpp_gcc_base:$1 