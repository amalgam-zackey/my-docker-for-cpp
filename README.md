# Supported
- [Eigen](https://gitlab.com/libeigen/eigen)
- [OpenCV](https://github.com/opencv/opencv)
- [Dear ImGui](https://github.com/ocornut/imgui)
- [pybind11](https://github.com/pybind/pybind11)
- GCC

# Reference Web Page
- https://qiita.com/ryoya-s/items/ee1daf9cab18c100c990
- https://qiita.com/fukushima1981/items/69c653e09f553b2d4134
- https://qiita.com/nishiys/items/1585d26a824862eec36b
- https://qiita.com/lucidfrontier45/items/872c38b5eec08e3800d7
- https://qiita.com/syoyo/items/676d1e1c07aef26a49e2
- https://qiita.com/kndt84/items/9524b1ab3c4df6de30b8